using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestDesarrollo.Models;

namespace TestDesarrollo.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class ProyectoController : Controller
    {
        private TestContext _context; 
        //GET: /<controller>/
        public ProyectoController(TestContext context){
            _context = context;
        }
        public IActionResult Project()
        {
            TestContext context = HttpContext.RequestServices.GetService(typeof(TestDesarrollo.Models.TestContext)) as TestContext;

            return View(_context.GetAllProjects());
        }

        public IActionResult Index()
        {
            return View();
        }
        
    }

}