using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace TestDesarrollo.Models
{
    public class TestContext
    {
        public string ConnectionString { get; set;}
        public TestContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<Proyecto> GetAllProjects()
        {
            List<Proyecto> list = new List<Proyecto>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM proyectos", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Proyecto()
                        {
                            Id = reader.GetInt16("IdProject"),
                            NombreProyecto = reader.GetString("NombreP"),
                            FechaInicio = reader.GetString("FechaInicio"),
                            FechaFin = reader.GetString("FechaFin"),
                            HorasProgramadas = reader.GetInt16("HorasP"),
                            HorasConsumidas = reader.GetInt16("HorasC"),
                            Estado = reader.GetBoolean("Estado")
                        });
                    }
                }
            }
            return list;
        }
    }
}