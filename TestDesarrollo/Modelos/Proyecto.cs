using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestDesarrollo.Models
{
    public class Proyecto
    {
        private TestContext context;
        public int Id {get;set;}
        public string NombreProyecto { get; set;}
        public string FechaInicio {get; set;}
        public string FechaFin {get; set;}
        public int HorasProgramadas {get; set;}
        public int HorasConsumidas {get; set;}
        public bool Estado {get; set;}
    }
}